# An open source codecademy.com-ish project for creating programming courses.
### Code runs on Skulpt (in a web-worker) -- in-browser Python 2.6 interpreter
### Currently you can only run arbitrary Python code -- no exercises yet.

## Documentation
   - [Materials structure](./docs/exercises.md)

## Backend:
   - django
   - django-rest-framework
   - django-rest-auth

## Frontend:
   - webpack
   - React v16
   - React Router 4
   - Redux
   - reactstrap (Bootstrap 4 components)


## How to use it

Clone the repo

## Kickstart the backend:

cd to django_backend directory

create your virtualenv and activate (python3) 

run `pip install -r requirements.txt` to install all django dependencies.

run `python manage.py migrate` to migrate database.

run `python manage.py runserver` to start django development server to serve the demo site.

the backend server should be localhost:8000.

## Kickstart the Frontend:

all JavaScript and html source code are within react_src directory, bundle.js will be generated in 
static/js directory. index.html will be generated in templates/project directory.

run `npm install` to install all node dependencies.

run `npm run start` to start the webpack dev server for frontend app.

now you can go to localhost:8083 to access the main page of the demo site.

## License

MIT

## Credits:
### The project is based on this boilerplate: https://github.com/ZachLiuGIS/reactjs-auth-django-rest/