import Sk from "skulpt"


const readModule = module => (
    Sk.builtinFiles["files"][module]
)

const sendMessage = (name, message) => {
    self.postMessage(JSON.stringify({
        type: name,
        data: message
    }));
}

const run = code => {
    Sk.configure({
        read: readModule,
        output: function (output) {
            sendMessage('stdout', output);
        }
    });
    try {
        Sk.importMainWithBody("<stdin>", false, code);
    } catch (e) {
        sendMessage('stderr', {
            exception: e.tp$name,
            description: String(e)
        });
    }
    sendMessage('exit');
}

self.onmessage = function(e) {
    run(e.data);
}
