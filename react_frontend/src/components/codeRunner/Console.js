import React from "react";


class FakeConsole extends React.Component{

    renderError(errorObj) {
        return (
            <div className="stderr">
                <span className="prompt">>>> </span>
                {errorObj.description}
            </div>
        )
    }

    renderResult() {
        const errorObj = this.props.errorObj;
        if (errorObj) return this.renderError(errorObj)
        
        const templ = (output, ind) => <div key={ind}>{'>>> ' + output}</div>;
        return this.props.lines.map((output, ind) => templ(output, ind))
    }

    render() {
        return (
            <div className="console">
            { this.props.lines.length > 0 || this.props.errorObj
                ? this.renderResult()
                : <span>>>> </span>
            }
            </div>
        )
    }
}


export default FakeConsole;
