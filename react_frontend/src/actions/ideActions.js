import { IDETypes } from "../constants/actionTypes";


export function write(chunk) {
    if (chunk && chunk.charCodeAt() === 10) {
        return breakLine();
    }
    return { type: IDETypes.WRITE_TERMINAL, chunk };
}
  
export function error(line) {
    return { type: IDETypes.ERROR_TERMINAL, line };
}

export function flush() {
    return { type: IDETypes.FLUSH_TERMINAL };
}

export function exit(worker) {
    return { type: IDETypes.EXIT_TERMINAL, worker: worker };
}

function breakLine() {
    return { type: IDETypes.BREAK_TERMINAL };
}